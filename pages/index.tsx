import { Card } from "semantic-ui-react";
import Movie from "../components/Movie/Movie";
import Test from "../components/Movie/Movie";
import Movies from "../components/Movies/Movies";
import { getMovies, getGenres } from "../utils/movies";

type HomeProps = {
  movies: Movie[];
  genres: Genre[];
};

export default function Home({ movies, genres }: HomeProps) {
  return <Movies {...{ movies, genres }} />;
}

export async function getStaticProps() {
  const movies = getMovies();
  const genres = getGenres();
  return {
    props: {
      movies,
      genres,
    },
  };
}
