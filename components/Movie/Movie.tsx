import styles from "./movie.module.scss";
import { Icon } from "semantic-ui-react";

type MovieProps = {
  movie: Movie;
  genres: Genre[];
};

export default function Movie({ movie, genres }: MovieProps) {
  return (
    <div className={styles.movie}>
      <div className={styles.header}>
        <h2>
          <Icon name="film" />
          {movie.title}
        </h2>
        <span>{movie.year}</span>
      </div>
      <div className={styles.description}>{movie.description}</div>
      <div className={styles.footer}>
        <div className={styles.tags}>
          {movie.genreIds
            .map((id) => genres.find((g) => g.id === id).name)
            .map((genre) => (
              <span className={styles.genre} key={genre}>
                {genre}
              </span>
            ))}
        </div>
        <div className={styles.votes}>
          votes:
          <span>{movie.votes}</span>
        </div>
      </div>
    </div>
  );
}
