import { Dispatch, SetStateAction } from "react";
import { Label, Segment } from "semantic-ui-react";
import { Group } from "../../typings/enums";
import { getUniqueYears } from "../../utils/movies";
import styles from "./filters.module.scss";
import LabelFilter from "./LabelFilter";

type FiltersProps = {
  filters: Filter[];
  setFilters: Dispatch<SetStateAction<Filter[]>>;
  amountVisible: number;
  amountTotal: number;
};

export default function Filters({
  filters,
  setFilters,
  amountVisible,
  amountTotal,
}: FiltersProps) {
  function filterExists(name: string | number, group: Group) {
    return (
      filters.find((f) => f.name === name && f.group === group) !== undefined
    );
  }

  function addFilter(name: string | number, group: Group, fnc: Function) {
    setFilters((currentFilters) => [...currentFilters, { name, group, fnc }]);
  }

  function removeFilter(name: string, group: Group) {
    setFilters((currentFilters) =>
      currentFilters.filter((f) => !(f.name === name && f.group === group))
    );
  }

  function toggleFilter(name: string | number, group: Group, fnc: Function) {
    if (filterExists(name, group)) {
      removeFilter.apply(null, arguments);
    } else {
      addFilter.apply(null, arguments);
    }
  }

  return (
    <Segment>
      <div className={styles.content}>
        <div className={styles.left}>
          <div className={styles.row}>
            Release date:
            {getUniqueYears().map((year) => (
              <LabelFilter
                key={year}
                text={year}
                active={filterExists(year, Group.YEAR)}
                onClick={() =>
                  toggleFilter(year, Group.YEAR, (m: Movie) => m.year === year)
                }
              />
            ))}
          </div>
          <div className={styles.row}>
            Genres:
            {[
              { id: 14, name: "Fantasy" },
              { id: 16, name: "Animation" },
              { id: 10751, name: "Family" },
              { id: 27, name: "Horror" },
              { id: 53, name: "Thriller" },
              { id: 35, name: "Comedy" },
              { id: 80, name: "Crime" },
            ].map((genre) => (
              <LabelFilter
                key={genre.name}
                text={genre.name}
                secondaryColor
                active={filterExists(genre.name, Group.GENRE)}
                onClick={() =>
                  toggleFilter(
                    genre.name,
                    Group.GENRE,
                    (m: Movie) => m.genreIds.indexOf(genre.id) !== -1
                  )
                }
              />
            ))}
          </div>
        </div>
        <div className={styles.right}>
          <Label size="tiny" className={styles.label}>
            Visible movies:
            <Label.Detail>{`${amountVisible} of ${amountTotal}`}</Label.Detail>
          </Label>
        </div>
      </div>
    </Segment>
  );
}
