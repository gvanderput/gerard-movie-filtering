import { useState } from "react";
import { Segment } from "semantic-ui-react";
import { applyFilters } from "../../utils/filtering";
import Filters from "../Filters/Filters";
import Movie from "../Movie/Movie";
import styles from "./movies.module.scss";

export default function Movies({ movies, genres }: MoviesProps) {
  const [filters, setFilters] = useState<Filter[]>([]);

  let shownMovies = applyFilters(movies, filters);

  return (
    <>
      <Filters
        {...{
          filters,
          setFilters,
          amountVisible: shownMovies.length,
          amountTotal: movies.length,
        }}
      />
      <Segment className={styles.segment}>
        <div className={styles.movies}>
          {shownMovies.map((movie) => (
            <Movie key={movie.title} {...{ movie, genres }} />
          ))}
        </div>
      </Segment>
    </>
  );
}
