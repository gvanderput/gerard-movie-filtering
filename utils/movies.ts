import movies from "../data/movies.json";
import { genres } from "../data/genres.json";

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

export const getGenres = () => {
  return genres;
};

export const getMovies = () => {
  return movies;
};

export const getUniqueYears = () => {
  return getMovies()
    .map((m) => m.year)
    .filter(onlyUnique)
    .sort((a, b) => (a > b ? -1 : 1));
};
