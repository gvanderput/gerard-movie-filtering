import { Group } from "../typings/enums";

function isShownByYear(movie: Movie, filters: Filter[]) {
  const yearFilters = filters.filter((filter) => filter.group === Group.YEAR);
  if (!yearFilters.length) return true;
  return yearFilters.some((filter) => filter.fnc(movie));
}

function isShownByGenre(movie: Movie, filters: Filter[]) {
  const genreFilters = filters.filter((filter) => filter.group === Group.GENRE);
  if (!genreFilters.length) return true;
  return genreFilters.every((filter) => filter.fnc(movie));
}

export function applyFilters(movies: Movie[], filters: Filter[]) {
  return movies.filter((movie) => {
    const showByYear = isShownByYear(movie, filters);
    const showByGenre = isShownByGenre(movie, filters);
    return showByYear && showByGenre;
  });
}
